import { add, has } from './types';

let id = 0;

const identity = (arg) => arg;

const normalize = (dispatchOrStore) => {
    if (dispatchOrStore && typeof dispatchOrStore.dispatch === 'function') {
        return dispatchOrStore.dispatch;
    } else {
        return dispatchOrStore;
    }
}

const normalizeAll = (dispatchOrStores) => {
    if (Array.isArray(dispatchOrStores)) {
        return dispatchOrStores.map(normalize);
    } else {
        return normalize(dispatchOrStores);
    }
}

export default function createAction(namespace, typeid, description, payloadReducer, metaReducer, clone = false) {
    
    // ####
    // Add Namespace-Feature with fallback to default behavior:
    // ####
    // Added 'typeid' to arguments. This should always be a SERIALIZABLE_ACTION_NAME. 
    // If Namespace-Feature is used, a typeid has to be specified as well. Else fallback to default behavior.
    var hasNamespace;
    if(!(hasNamespace = Object.prototype.toString.call(namespace) === '[object Array]')){	
        description = namespace;
        payloadReducer = typeid;
        metaReducer = description;
        namespace = undefined;
        typeid = undefined;
    }
    var typeidCorrect;
    if(hasNamespace && !(typeidCorrect = ((typeof typeid === 'string') && /^[0-9A-Z_@]+$/.test(typeid))))
    {
         throw new TypeError(
         	`Namespace-Feature is used with invalid TypeId. TypeId has to be in SERIALIZABLE_FORMAT. TypeId is ${typeid}`
         );
    }
    // ####
    
    if (typeof description === 'function') {
        metaReducer = payloadReducer;
        payloadReducer = description;
        description = undefined;
    }

    if (typeof payloadReducer !== 'function') {
        payloadReducer = identity;
    }

    if (typeof metaReducer !== 'function') {
        metaReducer = undefined;
    }

    const isSerializable = typeidCorrect || ((typeof description === 'string') && /^[0-9A-Z_]+$/.test(description));

    // If namespace is used, allow multiple "SERIALIZABLE"-Actions as long as it is unique to each single namespace
    if (isSerializable && !clone) {
        let key = hasNamespace ? namespace.join("\\")+"::"+typeid : description;
        if (has(key)) {
            throw new TypeError(`Duplicate action type: ${key}`);
        }

        add(key);
    } else {
        ++id;
    }

    const type = typeidCorrect ? namespace.join("\\")+"::"+typeid : (
            isSerializable ? description : `[${id}]${description ? ' ' + description : ''}`
        );
    
    
    let dispatchFunctions = undefined;

    function makeAction(...args) {
        if (metaReducer) {
            return {
                type,
                namespace: hasNamespace ? namespace : [], // Pass namespace to created action object
                payload: payloadReducer(...args),
                meta: metaReducer(...args),
	            description: (typeof description === 'string') ? description : ''
            };
        }

        return {
            type,
            namespace: hasNamespace ? namespace : [], // Pass namespace to created action object
            payload: payloadReducer(...args),
            description: (typeof description === 'string') ? description : ''
        };
    }

    const makeAndDispatch = (dispatchs) => (...args) => {
        if (Array.isArray(dispatchs)) {
            const payloadedAction = makeAction(...args);
            return dispatchs.map(dispatch => dispatch(payloadedAction));
        } else if (dispatchs) {
            return dispatchs(makeAction(...args));
        } else {
            return makeAction(...args);
        }
    }

    function actionCreator(...args) {
        return makeAndDispatch(dispatchFunctions)(...args);
    }

    actionCreator.getType = () => type;
    actionCreator.toString = () => type;

    actionCreator.raw = makeAction;
    actionCreator.unlinked = () => createAction(namespace, typeid, description, payloadReducer, metaReducer, true);

    let _dispatchOrStores = null;

    actionCreator.assignTo = (dispatchOrStores) => {
        _dispatchOrStores = dispatchOrStores;

        dispatchFunctions = normalizeAll(dispatchOrStores);

        if(hasNamespace && !actionCreator.isSubAction()){
            actionCreator.REQ.assignTo(dispatchOrStores);
            actionCreator.RES.assignTo(dispatchOrStores);
            actionCreator.ERR.assignTo(dispatchOrStores);
            actionCreator.CANCEL_REQ.assignTo(dispatchOrStores);
            actionCreator.CANCEL_RES.assignTo(dispatchOrStores);
        }

        return actionCreator;
    };

    actionCreator.assigned = () => !!dispatchFunctions;
    actionCreator.bound = () => false;
    actionCreator.dispatched = actionCreator.assigned;

    actionCreator.bindTo = (dispatchOrStores) => {
        _dispatchOrStores = dispatchOrStores;

        const boundActionCreator = makeAndDispatch(normalizeAll(dispatchOrStores));
        boundActionCreator.raw = makeAction;
        boundActionCreator.getType = actionCreator.getType;
        boundActionCreator.toString = actionCreator.toString;
        boundActionCreator.assignTo = () => boundActionCreator;
        boundActionCreator.bindTo = () => boundActionCreator;
        boundActionCreator.assigned = () => false;
        boundActionCreator.bound = () => true;
        boundActionCreator.dispatched = boundActionCreator.bound;
        return boundActionCreator;
    };

    actionCreator.asInstance = (instanceId) => {

        if(hasNamespace){
            let instanceNamespace;

            if(namespace.length > 0){
                instanceNamespace = [...namespace];
                instanceNamespace[instanceNamespace.length-1] = instanceNamespace[instanceNamespace.length-1]+"[instance:"+instanceId+"]";
            }
            else{
                instanceNamespace = ["[instance:"+instanceId+"]"];
            }
            
            const instanceAction = createAction(instanceNamespace,typeid,description,payloadReducer,metaReducer,clone);
            if(_dispatchOrStores !== null){
                instanceAction.assignTo(_dispatchOrStores);
            }
            return instanceAction;
        }
        else{
            throw new Error("Namespace-feature must be used in order to add an instance-suffix!");
        }

    }

    const REQ_SUFFIX = "@REQUEST";
    const RES_SUFFIX = "@RESPONSE";
    const ERR_SUFFIX = "@ERROR";
    const CANCEL_REQ_SUFFIX = "@CANCEL_REQUEST";
    const CANCEL_RES_SUFFIX = "@CANCEL_RESPONSE";

    actionCreator.isSubAction = () => {
        return typeid !== undefined && (
        (typeid.length > REQ_SUFFIX.length && typeid.indexOf(REQ_SUFFIX) === typeid.length-REQ_SUFFIX.length) ||
        (typeid.length > RES_SUFFIX.length && typeid.indexOf(RES_SUFFIX) === typeid.length-RES_SUFFIX.length) ||
        (typeid.length > ERR_SUFFIX.length && typeid.indexOf(ERR_SUFFIX) === typeid.length-ERR_SUFFIX.length) ||
        (typeid.length > CANCEL_REQ_SUFFIX.length && typeid.indexOf(CANCEL_REQ_SUFFIX) === typeid.length-CANCEL_REQ_SUFFIX.length) ||
        (typeid.length > CANCEL_RES_SUFFIX.length && typeid.indexOf(CANCEL_RES_SUFFIX) === typeid.length-CANCEL_RES_SUFFIX.length) );
    }

    if(hasNamespace && !actionCreator.isSubAction())
    {        
        actionCreator.REQUEST = actionCreator.REQ = (function(){
            return createAction(namespace,typeid+REQ_SUFFIX,description,payloadReducer,metaReducer,clone);
        })();
        actionCreator.RESPONSE = actionCreator.RES = (function(){
            return createAction(namespace,typeid+RES_SUFFIX,description,payloadReducer,metaReducer,clone);
        })();
        actionCreator.ERROR = actionCreator.ERR = (function(){
            return createAction(namespace,typeid+ERR_SUFFIX,description,payloadReducer,metaReducer,clone);
        })();
        actionCreator.CANCEL_REQUEST = actionCreator.CANCEL_REQ = (function(){
            return createAction(namespace,typeid+CANCEL_REQ_SUFFIX,description,payloadReducer,metaReducer,clone);
        })();
        actionCreator.CANCEL_RESPONSE = actionCreator.CANCEL_RES = (function(){
            return createAction(namespace,typeid+CANCEL_RES_SUFFIX,description,payloadReducer,metaReducer,clone);
        })();
    }


    return actionCreator;
};
