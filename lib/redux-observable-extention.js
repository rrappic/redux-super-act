import { ActionsObservable } from "redux-observable";
import { filter } from "rxjs/operators/filter";

ActionsObservable.prototype.isAction = function (matchAction) {
    return ActionsObservable.prototype.pipe.call(this, filter(passingAction => {
        return matchAction.getType() === passingAction.type;
    }));
};

/*
Not yet implemented

ActionsObservable.prototype.isActionNamespace = function(matchAction){
    return ActionsObservable.prototype.filter.call(this, function (passingAction) {
        return false;
    });
};
*/